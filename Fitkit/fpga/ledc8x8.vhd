-- Autor reseni: Mat�j Kudera (xkuder04)

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;

entity ledc8x8 is
port (  -- Sem doplnte popis rozhrani obvodu.

        RESET : in std_logic; -- pro asynchronní inicializace hodnot registrů
        SMCLK : in std_logic; -- clock frekvence pro celou entitu/ 7.3728 MHz perioda 135.6 ns
        ROW : out std_logic_vector (7 downto 0); -- Vektor pro aktivaci příslušného řádku na displeji. log. 1
        LED : out std_logic_vector (7 downto 0) -- Vektor pro aktivaci příslušné ledky na řádku displeje. log. 0
);
end ledc8x8;

architecture main of ledc8x8 is
    -- Sem doplnte definice vnitrnich signalu.

    signal change_line : std_logic := '0'; -- signál pro zmněnu aktivního řádku s ledkami
    signal cnt_line_change : std_logic_vector (7 downto 0); -- counter pro zm�nu ��dku SMCLK/256, pot�eba 8 bit
    signal cnt_on_off_time : std_logic_vector (21 downto 0); -- counter pro po��t�n� 0,5s
    signal clear_cnt : std_logic := '0'; -- reset po��tadla 0,5s

    signal enable_leds : std_logic := '1'; -- Zapne/vypne pole ledek
    signal stop_change : std_logic := '0'; -- Ponech� zapnut� display

    signal active_row : std_logic_vector (7 downto 0) := "00000001"; -- Výchozí stav. Aktivní první řádek
    signal active_leds : std_logic_vector (7 downto 0) := "11111111"; -- Výchozí stav ledek, vše vypnuto
    
begin
    -- Sem doplnte popis obvodu. Doporuceni: pouzivejte zakladni obvodove prvky
    -- (multiplexory, registry, dekodery,...), jejich funkce popisujte pomoci
    -- procesu VHDL a propojeni techto prvku, tj. komunikaci mezi procesy,
    -- realizujte pomoci vnitrnich signalu deklarovanych vyse.

    -- DODRZUJTE ZASADY PSANI SYNTETIZOVATELNEHO VHDL KODU OBVODOVYCH PRVKU,
    -- JEZ JSOU PROBIRANY ZEJMENA NA UVODNICH CVICENI INP A SHRNUTY NA WEBU:
    -- http://merlin.fit.vutbr.cz/FITkit/docs/navody/synth_templates.html.

    -- Nezapomente take doplnit mapovani signalu rozhrani na piny FPGA
    -- v souboru ledc8x8.ucf.

    -- Po��tadlo frekvence SMCLK/256
    process(RESET, SMCLK, enable_leds)
    begin
        if (RESET = '1') then
            cnt_line_change <= (others => '0');
        elsif (enable_leds = '0') then
	    cnt_line_change <= (others => '0'); -- Zastaven� po��t�n� kdy� ledky nesv�t�, nen� to pot�eba
        elsif (SMCLK'event) and (SMCLK = '1') then
	    cnt_line_change <= cnt_line_change + 1;
        end if;
    end process;
 
    -- Po��tadlo 0,5s
    process(RESET, SMCLK)
    begin
        if (RESET = '1') then
            cnt_on_off_time <= (others => '0');
	    enable_leds <= '1';
    	    stop_change <= '0';	    
        elsif (SMCLK'event) and (SMCLK = '1') then
	    if (clear_cnt = '1') then
	        cnt_on_off_time <= (others => '0');

		-- Zastaven� zm�ny zapin�n� vyp�n�n� displaye
		if (stop_change = '1') then
		    enable_leds <= '1';
		else
		    enable_leds <= '0';
		    stop_change <= '1';
		end if;
	    else
            	cnt_on_off_time <= cnt_on_off_time + 1;
	    end if;
        end if;
    end process;
        
    -- Posuvný registr pro změnu aktivního řádku řádku
    process(RESET, change_line)
    begin
        if (RESET = '1') then
            active_row <= "00000001"; -- po resetu se nastaví jako aktivní první řádek
        elsif (change_line'event) and (change_line = '1') then
            if (enable_leds = '1') then
                active_row <= active_row(6 downto 0) & active_row(7);
            end if;
        end if;
    end process;

    -- Nastavení aktivních ledek na řádku
    -- Snad to funguje i mimo enable_leds?
    process(RESET, active_row, enable_leds)
    begin
        if (RESET = '1') then
            active_leds <= "11111111"; -- po resetu se vypnou všechny ledky
	elsif (enable_leds = '0') then
	    active_leds <= "11111111";
        else
            -- Vybrání správných ledek na řádcích pro vypsání MK
            case active_row is
                when "00000001" =>
                    active_leds <= "11101110";
                when "00000010" =>
                    active_leds <= "11100100";
                when "00000100" =>
                    active_leds <= "11101010";
                when "00001000" =>
                    active_leds <= "01101110";
                when "00010000" =>
                    active_leds <= "10101110";
                when "00100000" =>
                    active_leds <= "11001111";
                when "01000000" =>
                    active_leds <= "10101111";
                when "10000000" =>
                    active_leds <= "01101111";
                when others =>
                    active_leds <= "11111111";
             end case;
        end if;
    end process;

    -- Reset po��tadla po dos�hnut� �asu 0,5s
    clear_cnt <= '1' when cnt_on_off_time = "1110000100000000000000" else '0';

    -- Zm�na ��dku
    change_line <= '1' when cnt_line_change = "11111111" else '0';

    -- Nastavení výstupu entity
    ROW <= active_row;
    LED <= active_leds;
end main;




-- ISID: 75579
